/** SETTINGS */
const PORT = 3555

/** LIBS */
const io = require('socket.io')(PORT)
const ss = require('socket.io-stream')

io.on('connection', (socket) => {

    const speech = require('@google-cloud/speech')

    const speechClient = new speech.SpeechClient()

    const encoding = 'LINEAR16'
    const sampleRateHertz = 16000

    let request = {
        config: {
            encoding,
            sampleRateHertz,
            languageCode: null
        },
        interimResults: false
    }

    // Language has changed
    socket.on('LANGUAGE_SELECT', (language) => {
        console.log('Selecting language')
        request.config.languageCode = language
    })

    const recongnizeStream = speechClient
        .streamingRecognize(request)
        .on('error', console.error)
        .on('data', (data) => {
            if (data.results[0] && data.results[0].alternatives[0]) {
                socket.emit('TRANSCRIPT', data.results[0].alternatives[0].transcript)
            } else {
                socket.emit('TIME_LIMIT', 'Time limit')
            }

            console.log(
                `Transcription: ${data.results[0].alternatives[0].transcript}`
            )
        })

    ss(socket).on('START', (stream) => {
        console.log('Starting stream')
        stream.pipe(recongnizeStream)
        //stream.pipe(process.stdout)
        //fs.createReadStream(filename).pipe(recongnizeStream)
    })

    socket.on('STOP', () => {
        console.log('Disconnect')
    })
    
    console.log('Connected')
})