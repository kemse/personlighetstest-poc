/** MODULES */
const axios = require('axios')
const queryString = require('query-string')

/** SETTINGS */
const apiUrl = 'http://ontologi.arbetsformedlingen.se/ontology/v1'


export default {
    searchOccupations (concept) {
        const values = {
            limit: 10,
            type: 'occupation',
            concept
        }

        return new Promise((resolve, reject) => {
            axios.get(apiUrl + '/concept/related?' + queryString.stringify(values)).then((response) => {
                resolve(response.data.relations)
            }).catch((error) => {
                reject(error)
            })
        })
    },

    getTraitsFromText (text) {
        const values = {
            body: text
        }

        return new Promise((resolve, reject) => {
            axios.post(apiUrl + '/text-to-structure', values).then((response) => {
                resolve(response.data.map((trait) => trait.name))
            }).catch((error) => {
                reject(error)
            })
        })
    }
}