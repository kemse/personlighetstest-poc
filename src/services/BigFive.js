/** MODULES */
const { getItems } = require('@alheimsins/b5-johnson-120-ipip-neo-pi-r')
const scoring = require('@alheimsins/bigfive-calculate-score')

const traits = {
    C: ['Duglighet', 'Ordningsamhet', 'Plikttrogenhet', 'Prestationssträvan', 'Självdisciplin', 'Eftertänksamhet'],
    E: ['Tillgivenhet', 'Sällskaplighet', 'Självhävdelse', 'Vitalitet', 'Spänningssökande', 'Gladlynthet'],
    O: ['Fantasi', 'Estetik', 'Känslor', 'Aktiviteter', 'Tankar', 'Värderingar'],
    A: ['Tillit', 'Rättframhet', 'Osjälviskhet', 'Följsamhet', 'Blygsamhet', 'Ömsinthet']
}

export default {
    getQuestions ({lang = 'sv', limit = 120} = {}) {
        return getItems(lang).splice(0, limit)
    },

    calulateScore ({lang = 'sv', test = '50-IPIP-NEO-PI-R', totalQuestions = 120, answers = []} = {}) {
        return scoring({lang, test, totalQuestions, answers})
    },

    getTraitsFromScores (scores) {
        let possibleTraits = []

        for (let k in scores) {
            if (scores[k].result === 'high') {
                possibleTraits = possibleTraits.concat(traits[k])
            }
        }

        return possibleTraits
    }
}