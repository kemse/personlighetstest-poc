/** MODULES */
const axios = require('axios')
const queryString = require('query-string')

/** SETTINGS */
const API_KEY = 'c2ViYXN0aWFuLmtlbWlAYXJiZXRzZm9ybWVkbGluZ2VuLnNl'
const apiUrl = 'https://dev-open-api.dev.services.jtech.se'

/** */
const occupationGroups = require('../data/concept_to_taxonomy.json')

const searchOccupationName = (name) => {
    const values = {
        limit: 1,
        type: 'occupation-name',
        q: name,
    }

    return new Promise((resolve, reject) => {
        axios.get(apiUrl + '/taxonomy/search?' + queryString.stringify(values), { headers: {'api-key': API_KEY}}).then((response) => {
            resolve(response.data.result.pop())
        }).catch((error) => {
            reject(error)
        })
    })
}


export default {
    searchOccupation (name) {
        return new Promise((resolve) => {
            searchOccupationName(name).then((result) => {
                if (result) {
                    if (occupationGroups.hasOwnProperty(result.parentId)) {
                        const occupation = occupationGroups[result.parentId]
                        resolve({
                            name: occupation.label,
                            ssyk: occupation.legacyAmsTaxonomyId
                        })
                    }
                }

                resolve(false)
            }).catch(() => {
                resolve(false)
            })
        })
    }
}