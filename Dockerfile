# build stage
FROM node:lts-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# production stage
FROM bitnami/nginx:1.16.0-ol-7-r0 as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
COPY nginx.conf /opt/bitnami/nginx/conf/nginx.conf
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]